#include <stdio.h>
#include <stdlib.h>

#include "prime.h"

int main(int argc, char *argv[])
{
    int low;
    int high;
    low = atoi(argv[1]);
    high = atoi(argv[2]);

    gen(low, high);

    return 0;
}
