#include "prime.h"
#include <stdio.h>

void gen(int low, int high ){
    int temp = 0;
    if(low > high) {
        temp = high;
        high = low;
        low = temp;
    }
    int conc;
    int x;
    printf("Prime numbers between %d and %d: ",low, high);
    while (low < high) {
        conc = 0;
        for (x = 2; x <= low / 2; ++x) {
            if(low % x == 0) {
                conc = 1;
                break;
            }
        }
        if (conc == 0)
            printf("%d ",low);
            low++;
    }
}
